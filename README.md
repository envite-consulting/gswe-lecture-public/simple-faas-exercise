# Exercise Cloud, Part I

A simple _Hello World_ application, written in Java that can be deployed in _Google Functions_. 

Also see: [HTTP Tutorial (2nd gen) by Google Cloud](https://cloud.google.com/functions/docs/tutorials/http)

In order to get started with this tutorial, first clone this repository:
```bash
git clone https://gitlab.com/envite-consulting/gswe-lecture-public/simple-faas-exercise.git
cd simple-faas-exercise
```

## View the Code of the Function

An application can be deployed as _Google Function_ if it implements the `com.google.cloud.functions.HttpFunction` interface.

The application for this exercise is implemented in the Java class [src/main/java/functions/HelloWorld.java](src/main/java/functions/HelloWorld.java).

The corresponding test you can find at [src/test/java/functions/HelloWorldTest.java](src/test/java/functions/HelloWorldTest.java).

### Optional: Build and run the function locally

If you want, you can adjust the function before you deploy it.
In order to verify that it works, you can build and run it locally.
The only thing you need is [Java 17](https://www.azul.com/downloads/?version=java-17-lts&package=jdk#zulu) installed.

Build and test the function:
```bash
./mvnw clean install
```

Run the function locally:
```bash
./mvnw function:run -Drun.functionTarget=functions.HelloWorld
```

You can invoke the function by open http://localhost:8080/ on your web browser, or by running:

```bash
curl http://localhost:8080/
```

## Decide in which Region to deploy the Function

Use the Google [Cloud Region Picker](https://googlecloudplatform.github.io/region-picker/) to select a cloud region with a low carbon footprint that is as close as possible.  

## Deploy the Function

**IMPORTANT:** All commands in this tutorial are for the [bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) shell. If you are working on Windows, either adjust the commands accordingly, use [WSL](https://learn.microsoft.com/en-us/windows/wsl/install), or install and use [Cygwin](https://www.cygwin.com/) or [MinGW-w64](https://www.mingw-w64.org/) which provides Linux libraries and commands (including bash).

First download and install [Google Cloud CLI](https://cloud.google.com/sdk/docs/install).

Login with your username:
```bash
gcloud auth login
```

Set project which should be used to deploy the function:
```bash
export PROJECT_ID="$(gcloud projects list --filter='name:lecture-cloud-1' --format='value(PROJECT_ID)')"
gcloud config set project ${PROJECT_ID}
```

Deploy the function with the following command (you have to set a region ...):
```bash
export GCP_ACCOUNT="$(gcloud config get account)"
export GCP_USER="${GCP_ACCOUNT%@*}" # e.g. student00
export GCP_REGION="..." # choose with Google Cloud Region Picker
gcloud functions deploy ${GCP_USER}-java-http-function \
  --gen2 \
  --runtime=java17 \
  --region=${GCP_REGION} \
  --source=. \
  --entry-point=functions.HelloWorld \
  --memory=512MB \
  --trigger-http \
  --max-instances=10 \
  --allow-unauthenticated
```

You can delete the function with the following command if you are done with the exercise:
```bash
gcloud functions delete ${GCP_USER}-java-http-function \
  --gen2 \
  --region=${GCP_REGION}
```

## Invoke the Function

When the deployment of your _Cloud Function_ completed successfully, it shows the endpoint URL which you can use to invoke the function. 

You can also extract the URL of the function with the following command:
```bash
export GCP_FUNCTION_URL="$(gcloud functions describe ${GCP_USER}-java-http-function --gen2 --region=${GCP_REGION} --format="value(serviceConfig.uri)")"
```

Now, you can call the function with the command:
```bash
curl ${GCP_FUNCTION_URL}
```

You can also run the invocation in parallel and in a loop:
```bash
while true; do 
  curl -s ${GCP_FUNCTION_URL} &
  sleep 0.001
done
```

Note: The trailing ampersand directs the shell to run the command in the background, as a job, asynchronously.

Hint: If you deploy the function without `--allow-unauthenticated`, it requires authentication. You can authenticate with the following command:

``` bash
curl -H "Authorization: bearer $(gcloud auth print-identity-token)" ${GCP_FUNCTION_URL}
```

Optionally, you can use a load testing tool like [Artillery](https://www.artillery.io/) to execute a predefined scenario:

```bash
export GCP_IDENTITY_TOKEN="$(gcloud auth print-identity-token)"

artillery run client/artillery_scenario.yml -t ${GCP_FUNCTION_URL}
```

## Dashboard for Functions

### Login to Console and Select Project

Open https://console.cloud.google.com/ in your web browser and login with your credentials (e.g. _student00@gcp.ssa.envite.green_).

As first step you need to select a _project_. To do this, click on `Select a project` on the top left of the website.

![](doc/gcp_select_project.png)

Now you can first choose an organization. Click on `NO ORGANIZATION` on the pop-up windows which has been opened.
In the drop-down menu select `gcp.ssa.envite.green`.

![](doc/gcp_select_organization.png)

Now all projects on which you have permissions are listed.
Click on `lecture-cloud-1` to finally choose this project.

![](doc/gcp_select_lecture_project.png)

### Open Cloud Functions Dashboard

In order to open the _Cloud Functions_ dashboard click into the search bar on the top of the website and type `cloud functions`. Click on the `Cloud Functions` entry to open the dashboard. 

![](doc/gcp_open_functions_overview.png)

Now, the _Cloud Functions_ overview page is displayed.
On this page all _Cloud Functions_ deployed as part of this lecture are listed.

![](doc/gcp_functions_overview.png)

You can search for the _Cloud Function_ which you have deployed and click on it, to open the detailed view.

![](doc/gcp_functions_detail.png)

Here you can also track the memory utilization and the active instances with a short delay.

Invoke your functions in a loop to generate load. You can use the _while-loop_ shown in the previous section to do this.
What can you observe in the dashboard? Can you also identify further optimization potential when deploying the function?  
